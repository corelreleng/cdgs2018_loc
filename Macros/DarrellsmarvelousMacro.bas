Private Function GetString(sentString As String)
Dim strFileName As String
Dim str1 As String, str2 As String, str3 As String
Dim iFileNbr As Integer

iFileNbr=FreeFile
strFileName = "D:\waldo\v1700\CGS17-DBC-JOB1\Loc\Macros\PassoloBaseFile.txt"
Open strFileName For Input As iFileNbr

Do Until EOF(iFileNbr)
	 Input #iFileNbr, str1
	 Input #iFileNbr, str2
	 Input #iFileNbr, str3
Loop

If sentString = "languageCode" Then
	GetString = str1
ElseIf sentString = "fileToOpen" Then
	GetString = str2
ElseIf sentString = "glossaryOutputFile" Then
	GetString = str3
Else
	GetString = "Error"
End If
Close iFileNbr

End Function
Private Function CreateGlossary(PrePost As String, sfPassoloFileToOpen As String)
Dim lang As PslLanguage
Dim prj As PslProject
Dim sLangCode As String

sFileName=""
sFileName = Mid(sfPassoloFileToOpen,InStrRev(sfPassoloFileToOpen,"\")+1, (InStrRev(sfPassoloFileToOpen,"."))-(InStrRev(sfPassoloFileToOpen,"\")+1)) 'parses out the filename from the whole path

'sGlossaryOutputLocation = GetString("glossaryOutputFile") & "\" & PrePost & "\" & "Glossary_" & sFileName & ".txt" (Can be removed after testing 09/05/2013)

PSL.Projects.Open(sfPassoloFileToOpen)
  Set prj = PSL.ActiveProject

  For Each lang In prj.Languages

    
    Dim bundle As PslTransBundle
    Set bundle = prj.PrepareTransBundle

    Dim trn As PslTransList
    Dim i As Integer

    For i = 1 To prj.TransLists.Count
      Set trn = prj.TransLists(i)
      sLangCode = lang.LangCode

      If trn.Language Is lang Then
        bundle.AddTransList(trn)
      End If
    Next i
    sGlossaryOutputLocation = GetString("glossaryOutputFile") & "\" & PrePost & "\" & "Glossary_" & sFileName & ".txt"

    If sFileName = "DrawX7_32" Or sFileName = "DrawX7_64" Then
    sGlossaryOutputLocation = GetString("glossaryOutputFile") & "\" & PrePost & "\" & Mid(sFileName,Len(sFileName)-1, 2) & "\" & "Glossary_" & sFileName & "_" & sLangCode & ".txt"
	End If

      prj.Export "Glossary Export" ,bundle, sGlossaryOutputLocation'filename

  Next lang

End Function
Private Function Validate (PrePost As String, sPassoloFileToOpen As String)

Dim sGlossaryOutputLocation As String
Dim sLangCode As String
Dim prj As PslProject
Dim trn As PslTransList
Dim checkwnd As PslOutputWnd
Dim sFileName As String

'sLangCode = GetString("languageCode")
'sPassoloFileToOpen = GetString("fileToOpen")
sFileName=""
sFileName = Mid(sPassoloFileToOpen,InStrRev(sPassoloFileToOpen,"\")+1, (InStrRev(sPassoloFileToOpen,"."))-(InStrRev(sPassoloFileToOpen,"\")+1))
sGlossaryOutputLocation = GetString("glossaryOutputFile") & "\" & PrePost & "\" &  "Validation_" & sFileName &".txt"

Open sGlossaryOutputLocation For Output As #2

' Create PASSOLO project
PSL.Projects.Open (sPassoloFileToOpen)
Set prj = PSL.ActiveProject
'If prj Is Nothing Then Exit Sub

Set prj = PSL.ActiveProject
Set trn = PSL.ActiveTransList
Set checkwnd = PSL.OutputWnd(pslOutputWndCheck)

For Each trn In prj.TransLists
	If trn.Title <> "" Then
		trn.Open
		trn.Check
	End If

	Print #2, vbNewLine & vbNewLine & "************************************************************************"
	Print #2, "Validation For ";trn.Title
	Print #2, "************************************************************************" & vbNewLine

	For Line = 1 To checkwnd.LineCount
  		If checkwnd.Type(Line) = pslOutputJump Then
		str1 = checkwnd.Text(Line)
		Print #2, LTrim(Mid(str1, InStr(str1, " "), Len(str1))) & vbCrLf;
		End If
	Next Line

	Next trn
PSL.Projects.Close(pslDoNotSaveChanges)
Close #2

End Function
Private Function Update (sFileName As String)
Dim sPassSourceList As PslSourceList
Dim sPassTransList As PslTransList

PSL.Projects.Open (sFileName)

PSL.ActiveProject.UpdateSourceLists
PSL.ActiveProject.UpdateTransLists

End Function
Private Function ResetVersion(sFileName As String)

PSL.Visible = False
PSL.Projects.Open (sFileName)
Set prj = PSL.ActiveProject
  If Not(prj Is Nothing) Then
  	'Loop thru all selected translation lists
  For Each trn In prj.TransLists
  trn.Open
   	 If trn.Selected Then
		w =trn.StringCount
	'Loop thru all strings
		For i = 1 To w
	   	Set t = trn.String(i)
   	    	If t.ResType = "Version" Then
	    		t.Text = t.SourceText
 	    		t.State(pslStateTranslated) = True
	 		End If
	   	Next i
	   	trn.Save
  End If
	    trn.Close
    	Next trn
End If

End Function
Private Function Error(sPassoloFileToOpen As String, iError As Integer)

Open sGlossaryOutputLocation For Output As #3
Print #3, "Error Code : " & iError & " " & sPassoloFileToOpen
Close #3

End Function
Private Function Generate(sFileName As String)

PSL.Projects.Open (sFileName)

For Each trn In PSL.ActiveProject.TransLists
	trn.GenerateTarget
Next trn

End Function
Sub Main
Dim sPassoloFileToOpen As String
Dim sLangCode As String
Dim sGlossaryOutputLocation As String
Dim prj As PslProject
Dim trn As PslTransList
Dim t As PslTransString
Dim i As Long
Dim w As Long

PSL.Visible = False
PSL.DisplayAlerts =pslAlertsNone

sPassoloFileToOpen = GetString("fileToOpen")  'returns the path of the Passolo file including the name of the file
sLangCode = GetString("languageCode")  'returns the language of the Passolo file

'CreateGlossary("Pre",sPassoloFileToOpen)
'Validate("Pre",sPassoloFileToOpen)
Update(sPassoloFileToOpen)
'ResetVersion(sPassoloFileToOpen)
'CreateGlossary("Post",sPassoloFileToOpen)
'Validate("Post",sPassoloFileToOpen)
Generate(sPassoloFileToOpen)

PSL.Projects.Close

End Sub

'Darrell MacKay (March 2013) : Almagamated all the smaller scripts into 1 bigger script. This reduces the amount of times Passolo has to be launched,
'reducing the chance of error.
'======================================================================================================================================================
'Darrell MacKay (March 2013) : Corrected the generate function to generate all languages of a multi language file.
'======================================================================================================================================================
'Darrell MacKay (April 2013) : Corrected the reset version function to trn.open was in the wrong location.
'======================================================================================================================================================
'Darrell MacKay (April 2013)
'CreateGlossary function : "PASSOLO Glossary Maker" changed to "Glossary Export" to accomodate extended characters as "Glossary Export"
'creates Unicode files and "PASSOLO Glossary Maker" creates ANSI.
'======================================================================================================================================================
'Darrell MacKay (September 2013)
'CreateGlossary function : Added in some code to write seperate glossaries for files containing more than one language (currently DrawXX_32.psl and DrawXX_64.psl)
