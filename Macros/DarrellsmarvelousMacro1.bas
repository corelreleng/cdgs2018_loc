'*************************************************************************************************************************
'MarvelousMacro.bas
'Written by Darrell MacKay April 2014
'It currently reads a file with all the passolo projects listed and creates a glossary for each file.
'For Passolo projects containing multiple languages it will create one multi language glossary
'*************************************************************************************************************************



Sub Main
Dim sWorkingFile As String
Dim sTimerLocation As String
Dim sBuildType As String
sWorkingFile = "D:\LocBuild\Macros\WorkingFileList.txt"


Open sWorkingFile For Input As #1

'Keep reading until the end of the file
Line Input #1,L$
sBuildType = L$

PSL.Visible = False
While Not EOF(1)
    Line Input #1,L$
    file = L$
PSL.Projects.Open file

If sBuildType ="FullBuild" Then
	CreateGlossary(file,"Pre")
	Validate(file, "Pre")
	Update(file)
	ResetVersion()
	CreateGlossary(file,"Post")
	Validate(file, "Post")
	Generate()

Else
	Update(file)
	Generate()
End If

PSL.Projects.Close

Wend

Close #1

End Sub
Private Function CreateGlossary(outputFile As String,PrePost As String)

Dim prj As PslProject
Dim file As String
Dim fileLocation As String
Dim sGlossaryFileName As String
Dim sBuildLogDir As String
sBuildLogDir = "D:\LocBuild\Logs\"

sGlossaryFileName = sBuildLogDir & "Glossary\"

fileLocation = sGlossaryFileName  & PrePost & "\" & (Mid(outputFile,InStrRev(outputFile,"\")+1, (InStrRev(outputFile,"."))-(InStrRev(outputFile,"\")+1))) & ".glo"
Set prj = PSL.ActiveProject
	If prj Is Nothing Then
		PSL.Output "No active project found."
		Exit Function'
	End If

'Create a bundle that the export function uses to create a glossary
 Dim bundle As PslTransBundle
    Set bundle = prj.PrepareTransBundle

    Dim trn As PslTransList
    Dim i As Integer
    For i = 1 To prj.TransLists.Count
      Set trn = prj.TransLists(i)
        bundle.AddTransList(trn)
    Next i

'Dim strFileName As String
Dim sCreatedFileLocation As String

prj.Export ("Glossary Export", bundle,sCreatedFileLocation & fileLocation, expAll)

End Function
Private Function Validate(outputFile2 As String, PrePost As String)
Dim fileLocation As String
Dim sValidateFileName As String
Dim sBuildLogDir As String
sBuildLogDir = "D:\LocBuild\Logs\"

sValidateFileName = sBuildLogDir & "Validate\"
Set prj = PSL.ActiveProject
Set lang = PSL.ActiveProject.Languages
Set trn = PSL.ActiveTransList
Set checkwnd = PSL.OutputWnd(pslOutputWndCheck)
Dim langcode As String

fileLocation = sValidateFileName  & PrePost & "\" & (Mid(outputFile2,InStrRev(outputFile2,"\")+1, (InStrRev(outputFile2,"."))-(InStrRev(outputFile2,"\")+1))) & langcode & ".txt"
Open fileLocation For Output As #2
	For Each trn In prj.TransLists
		If trn.Title <> "" Then
			trn.Open
			trn.Check
		End If
		Print #2, vbNewLine & vbNewLine & "************************************************************************"
		Print #2, "Validation For ";trn.Title; "   "; trn.Language.LangCode
		Print #2, "************************************************************************" & vbNewLine
		For Line = 1 To checkwnd.LineCount
  			If checkwnd.Type(Line) = pslOutputJump Then
			str1 = checkwnd.Text(Line)
			Print #2, LTrim(Mid(str1, InStr(str1, " "), Len(str1))) & vbCrLf;
			End If
		Next Line
	Next trn
	Close #2

End Function
Private Function Update(sPassoloFileName As String)
Dim sPassSourceList As PslSourceList
Dim sPassTransList As PslTransList

PSL.Projects.Open (sPassoloFileName)

PSL.ActiveProject.UpdateSourceLists
PSL.ActiveProject.UpdateTransLists
End Function
Private Function ResetVersion()
Set prj = PSL.ActiveProject
  If Not(prj Is Nothing) Then
  	'Loop thru all selected translation lists
  For Each trn In prj.TransLists
  trn.Open
   	 If trn.Selected Then
		w =trn.StringCount
	'Loop thru all strings
		For i = 1 To w
	   	Set t = trn.String(i)
   	    	If t.ResType = "Version" Then
	    		t.Text = t.SourceText
 	    		t.State(pslStateTranslated) = True
	 		End If
	   	Next i
	   	trn.Save
  End If
	    trn.Close  'does this need to be done??
    	Next trn
End If

End Function

Private Function Generate()
Set prj = PSL.ActiveProject

  If Not(prj Is Nothing) Then
  	'Loop thru all selected translation lists
  For Each trn In prj.TransLists
	trn.GenerateTarget

  Next trn
End If
trn.Close  'does this need to be done??
End Function

'Darrell MacKay (March 2013) : Almagamated all the smaller scripts into 1 bigger script. This reduces the amount of times Passolo has to be launched,
'reducing the chance of error.
'======================================================================================================================================================
'Darrell MacKay (March 2013) : Corrected the generate function to generate all languages of a multi language file.
'======================================================================================================================================================
'Darrell MacKay (April 2013) : Corrected the reset version function to trn.open was in the wrong location.
'======================================================================================================================================================
'Darrell MacKay (April 2013)
'CreateGlossary function : "PASSOLO Glossary Maker" changed to "Glossary Export" to accomodate extended characters as "Glossary Export"
'creates Unicode files and "PASSOLO Glossary Maker" creates ANSI.
'======================================================================================================================================================
'Darrell MacKay (September 2014) : Add langauge code to multilanguage validation reports.
'======================================================================================================================================================
'Darrell MacKay (March 2015) : Changed macro to read a list of files from a text file instead of reading the PassoloBaseFile. Also changed the macro to
'allow Final Builder to change the input and output locations for files.
